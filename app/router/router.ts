import { FastifyInstance } from "fastify"; 
import TransactionController  from "../controller/TransactionController";

export const router = async(app:FastifyInstance) => {
 // app.get('/', TransactionController.getall);
  app.post('/transactions', TransactionController.create);
};

