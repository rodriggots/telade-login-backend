import fastify from "fastify"; 
import { router } from "./router/router";

const app = fastify({ logger: true });

const PORT = 8000;

app.register(router);

app.listen({
  host: '0.0.0.0',
  port: PORT
}).then( () => {
  console.log(`servidor rodando em: http://localhost:${PORT}`); 
});