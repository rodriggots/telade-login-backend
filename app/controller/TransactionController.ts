import { FastifyRequest, FastifyReply } from 'fastify';
import { prismaClient } from '../model/PrismaModel';
import { z } from 'zod';

class TransactionController {
  async create(req: FastifyRequest, reply: FastifyReply) {
    const sechema = z.object({
      tipo: z.string(),
      data: z.string(),
      tipomoeda: z.string(),
      valor: z.string(),
      tipodetransacao: z.string(),
      descricao: z.string()
    });

    const {tipo, data, tipomoeda, valor, tipodetransacao, descricao } = sechema.parse(req.body);
    
    const trransactions = await prismaClient.transactions.create({
      data: {
        tipo,
        data,
        tipomoeda,
        valor,
        tipodetransacao,
        descricao
      }
    });
    return reply.send('cadastado');
  }
}

export default new TransactionController();
